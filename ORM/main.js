const express = require('express')
const bodyParser = require('body-parser')
const Sequelize = require('sequelize')
const alert=require('alert-node')
const window=require('window')
require('dotenv').config()
require('dotenv').config({silent: true})


const app = express()
app.use(bodyParser.json())

const request = require('request');
const uuidv4 = require('uuid/v4');
//zona translate
var key_var = 'ff7afd62537d45358bab676c7af0d82d';
var subscriptionKey = 'ff7afd62537d45358bab676c7af0d82d';

var endPoint ='https://api.cognitive.microsofttranslator.com/';

function translateText(text,language,callback){
    let options = {
        method: 'POST',
        baseUrl: endPoint,
        url: 'translate',
        qs: {
          'api-version': '3.0',
          'to': [language]
        },
        headers: {
          'Ocp-Apim-Subscription-Key': process.env.KEY_TRANSLATE,
          'Content-type': 'application/json',
          'X-ClientTraceId': uuidv4().toString()
        },
        body: [{
              'text': text
        }],
        json: true,
    };
    request(options, function(err, res, body){
        var json=JSON.stringify(body,null,4);
		var obj = JSON.parse(json);
		var result=obj[0]["translations"][0].text;
		callback(result);
	});
	
};


async function main(text,language){
	translateText(text,language,function(result){
		alert(result);	
	});
};

app.get('/authors/:aid/quotes/:qid/translate/:language', async (req, res) => {
	try{
		let authors = await Authors.findById(req.params.aid)
		console.log("Autorul: "+authors);
		if (authors){
			let quotes = await authors.getQuotes({where : {id : req.params.qid}})
			let quote = quotes.shift()
			console.log("Quote: "+quote.content);
			console.log("Language: "+req.params.language);
			await main(quote.content,req.params.language);
		}
		    res.status(201).json({message : 'traslated'})
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})



const sequelize = new Sequelize(process.env.DB,process.env.DB_USER,process.env.DB_PASSWORD,{
	dialect : 'mysql',
	define : {
		timestamps : false
	}
})

const Quotes = sequelize.define('quotes', {
	title : {
		allowNull : false,
		type : Sequelize.STRING,
		validate : {
			len : [3,100]
		}
	},
	date : {
		allowNull: false,
		defaultValue : Sequelize.NOW,
		type : Sequelize.DATE
	},
	content : {
		allowNull : false,
		type : Sequelize.TEXT,
		validate : {
			len : [10,1000]
		}		
	}
}, {
	underscored : true
})

const Authors = sequelize.define('authors', {
	name : {
		allowNull : false,
		type : Sequelize.STRING,
		validate : {
			len : [3,100]
		}		
	},
	surname : {
		allowNull : false,
		type : Sequelize.STRING,
		validate : {
			len : [3,100]
		}
	}
},{
	underscored : true
})

Authors.hasMany(Quotes)

app.get('/create', async (req, res) => {
	try{
		await sequelize.sync({force : true})
		res.status(201).json({message : 'created'})
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

app.get('/authors', async (req, res) => {
	try{
		let authors = await Authors.findAll()
		res.status(200).json(authors)
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

app.post('/authors', async (req, res, next) => {
	try{
		//await Authors.create(req.body)
        //res.status(201).json({message : 'created'})
        if (req.query.bulk && req.query.bulk == 'on'){
			await Authors.bulkCreate(req.body)
			res.status(201).json({message : 'created'})
		}
		else{
			await Authors.create(req.body)
			res.status(201).json({message : 'created'})
		}
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

app.get('/authors/:id', async (req, res) => {
	try{
		let authors = await Authors.findById(req.params.id)
		if (authors){
			res.status(200).json(authors)
		}
		else{
			res.status(404).json({message : 'not found'})
		}
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

app.put('/authors/:id', async (req, res) => {
	try{
		let authors = await Authors.findById(req.params.id)
		if (authors){
			await authors.update(req.body)
			res.status(202).json({message : 'accepted'})
		}
		else{
			res.status(404).json({message : 'not found'})
		}
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

app.delete('/authors/:id', async (req, res) => {
	try{
		let author = await Authors.findById(req.params.id)
		if (author){
			await author.destroy()
			res.status(202).json({message : 'accepted'})
		}
		else{
			res.status(404).json({message : 'not found'})
		}
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

app.get('/authors/:aid/quotes', async (req, res) => {
	try{
		let author = await Authors.findById(req.params.aid)
		if (author){
			let quotes = await author.getQuotes()
			res.status(200).json(quotes)
		}
		else{
			res.status(404).json({message : 'not found'})
		}
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

app.get('/authors/:aid/quotes/:qid', async (req, res) => {
	try{
		let author = await Authors.findById(req.params.aid)
		if (author){
			let quotes = await author.getQuotes({where : {id : req.params.qid}})
			res.status(200).json(quotes.shift())
		}
		else{
			res.status(404).json({message : 'not found'})
		}
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

app.post('/authors/:aid/quotes', async (req, res) => {
	try{
		let author = await Authors.findById(req.params.aid)
		if (author){
			let quotes = req.body
			quotes.author_id = author.id
			await Quotes.create(quotes)
			res.status(201).json({message : 'created'})
		}
		else{
			res.status(404).json({message : 'not found'})
		}
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

app.put('/authors/:aid/quotes/:qid', async (req, res) => {
	try{
		let author = await Authors.findById(req.params.aid)
		if (author){
			let quotes = await author.getQuotes({where : {id : req.params.qid}})
			let quote = quotes.shift()
			if (quote){
				await quote.update(req.body)
				res.status(202).json({message : 'accepted'})
			}
			else{
				res.status(404).json({message : 'not found'})
			}
		}
		else{
			res.status(404).json({message : 'not found'})
		}
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

app.delete('/authors/:aid/quotes/:qid', async (req, res) => {
	try{
		let author = await Authors.findById(req.params.aid)
		if (author){
			let quotes = await author.getQuotes({where : {id : req.params.qid}})
			let quote = quotes.shift()
			if (quote){
				await quote.destroy(req.body)
				res.status(202).json({message : 'accepted'})
			}
			else{
				res.status(404).json({message : 'not found'})
			}
		}
		else{
			res.status(404).json({message : 'not found'})
		}
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

app.listen(3000)